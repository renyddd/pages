;;; init-org-static-blog.el --- org-static-blog configurations -*- lexical-binding: t -*-
;;; Commentary: provide some closely related configurations.
;;; Code:

(setq org-static-blog-publish-title "Renyddd Blog")

;; my/org-static-blog-base-directory is defined in ~/.emacs.d
(setq org-static-blog-publish-directory my/org-static-blog-base-directory)

(setq org-static-blog-posts-directory
      (concat org-static-blog-publish-directory "posts/"))
(setq org-static-blog-drafts-directory
      (concat org-static-blog-publish-directory "drafts/"))

(setq org-static-blog-enable-tags t)
;; Table-Of-Contents
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)
(setq org-static-blog-use-preview t)
(setq org-static-blog-enable-tag-rss nil)
(setq org-static-blog-index-length 50)


(defun my/org-static-blog-template (tTitle tContent &optional tDescription)
  "Create the template that is used to generate the static pages."
  (concat
   "<!DOCTYPE html>\n"
   "<html lang=\"" org-static-blog-langcode "\">\n"
   "<head>\n"
   "<!-- Google tag (gtag.js) -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=G-NMJ18BDJG9\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-NMJ18BDJG9');
</script>\n"
   "<meta charset=\"UTF-8\">\n"
   (when tDescription
     (format "<meta name=\"description\" content=\"%s\">\n" tDescription))
   "<link rel=\"alternate\"\n"
   "      type=\"application/rss+xml\"\n"
   "      href=\"" (org-static-blog-get-absolute-url org-static-blog-rss-file) "\"\n"
   "      title=\"RSS feed for " org-static-blog-publish-url "\">\n"
   "<title>" tTitle "</title>\n"
   org-static-blog-page-header
   "</head>\n"
   "<body>\n"
   "<div id=\"preamble\" class=\"status\">"
   org-static-blog-page-preamble
   "</div>\n"
   "<div id=\"content\">\n"
   tContent
   "</div>\n"
   "<div id=\"postamble\" class=\"status\">"
   org-static-blog-page-postamble
   "</div>\n"
   "</body>\n"
   "</html>\n"))



;; This header is inserted into the <head> section of every page:
;;   (you will need to create the style sheet at
;;    ~/projects/blog/static/style.css
;;    and the favicon at
;;    ~/projects/blog/static/favicon.ico)
(setq org-static-blog-page-header
      "<meta name=\"author\" content=\"renyddd\">
<meta name=\"referrer\" content=\"no-referrer\">
<link href= \"static/style.css\" rel=\"stylesheet\" type=\"text/css\" />
<link rel=\"icon\" href=\"static/favicon.ico\">")

;; This preamble is inserted at the beginning of the <body> of every page:
;;   This particular HTML creates a <div> with a simple linked headline
(setq org-static-blog-page-preamble
      "<div class=\"header\">
  <a href=\"https://renyddd.top\">Renyddd Site</a>
  <nav>
    <ul>
      <li><a href=\"https://renyddd.top\">Home</a></li>
      <li><a href=\"https://renyddd.top/photo.html\">Photo</a></li>
      </li>
    <ul>
  </nav>
</div>")

;; This postamble is inserted at the end of the <body> of every page:
;;   This particular HTML creates a <div> with a link to the archive page
;;   and a licensing stub.
(setq org-static-blog-page-postamble
      "<div id=\"archive\">
  <a href=\"https://renyddd.top/archive.html\">Other posts</a>
</div>
<center><a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/3.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/3.0/88x31.png\" /></a><br /><span xmlns:dct=\"https://purl.org/dc/terms/\" href=\"https://purl.org/dc/dcmitype/Text\" property=\"dct:title\" rel=\"dct:type\">renyddd</span> by <a xmlns:cc=\"https://creativecommons.org/ns#\" href=\"https://renyddd.top\" property=\"cc:attributionName\" rel=\"cc:attributionURL\">Renyddd</a> is licensed under a <a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/3.0/\">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.</center>")

;;TODO: add extra tags here, eg: photo, tags, archive
;; 可以参考：http://alhassy.com/AlBasmala#org68d6157

;;; init-org-static-blog.el ends here
